# https://hub.docker.com/r/grafana/promtail/tags?name=2.
FROM grafana/promtail:2.7.0
COPY config.yaml /etc/promtail/config.yml
