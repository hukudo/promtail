# Promtail Deployment
Push your host's logs from `/var/logs/` and the Docker daemon to [Loki](https://grafana.com/docs/loki/latest/) (basic auth).
First, clone this repo:
```
git clone https://gitlab.com/hukudo/promtail.git
cd promtail/
```

## Promtail
```
cp share/env.sample .env
vi .env
docker-compose up -d --build --remove-orphans
```

## Docker
```
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
cp share/daemon.json /etc/daemon.json
vi /etc/daemon.json
systemctl restart docker
```
See also [Loki Docker Plugin Installation Docs](https://grafana.com/docs/loki/latest/clients/docker-driver/).

Be sure to re-create (`docker-compose down && docker-compose up`) your containers for logs to become available in Loki.
