# Maintenance

## Get default config
```
docker run -d --name promtail-default grafana/promtail:2.7.0
docker cp promtail-default:/etc/promtail/config.yml config.yaml
docker rm -f promtail-default
```
